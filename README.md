# Pauli Rule Model
![Aminah Dakka](https://img.shields.io/badge/Developer-Aminah%20Dakka-blue)
## WHAT IS IT?

This is a simplified model to demonstrate the distribution of electrons in an atom.
This model is based on Pauli's theory.
It's called "Electronic Configuration", and it's formula is: 2*n^2 which helps in the determination of the maximum number of electrons present in an orbit and the arrangement of those electrons where n = orbit number.

## HOW IT WORKS

Electrons are negatively charged subatomic particles arranged like a cloud of negative charges outside the nucleus of an atom. The arrangement depends on the levels that are known as 1, 2, 3, 4.. and the corresponding shells are known as K, L, M, N and so on.
For instance,
- 1st level - K shell/orbit contains 2 electrons.
- 2nd level - L shell/orbit contains 8 electrons.
- 3rd level - M shell/orbit contains 18 electrons and so on.

## HOW TO USE IT

The Draw Empty Atom button is self-explanatory.
This model has 2 variables: numOfElectrons, and speed which control the number and the speed of electrons spinning around the nucleus.
There are 2 extra examples to demostrate the operation of earning & losing electrons for both CL & NA elements.
For the output, we have added few known elements with a particular number of electrons.
The plot only demonstrates the speed change.

## THINGS TO NOTICE

Note that whenever an atom loses an electron it gains a positive charge, and whenever it earns an electron it gains a negative charge.


## EXTENDING THE MODEL

Can you add random energy to an electron revolving in the outermost orbit in order to make the operation of losing or earning an electron is more dynamic? 


## RELATED MODELS

Turtles Circling

## CREDITS AND REFERENCES

This model is a demonstration for an activity in the book of Physics and Chemistry for the 8th grade of the Syrian curriculum.  
```Unit 1: structural chemistry - Lesson 1: Atom & Element - Page 8.```

## License
MIT
